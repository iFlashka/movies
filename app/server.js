import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
const port = 8080;
const app = express();

// Обработка данных
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Логирование запросов
app.use(morgan('dev'));

app.get('/', (req, res) => {
  res.send('zxc');
});

// Запуск сервера
app.listen(port, () => {
  console.log(`Запущенно на порту: ${port}`);
});

export default app;